
/**
 * Expose `parse()`.
 */

module.exports = parse;

/**
 * Parse the given `str`.
 *
 * @param {String} str
 * @return {Array}
 * @api public
 */

function parse(str) {
  str = 'WHERE ' + str;

  return query();

  function accept(re, name) {
    var m = re.exec(str);
    if (!m) return;
    str = str.slice(m[0].length);
    return [name, m[1]];
  }

  /**
   * clause*
   */

  function query() {
    var clauses = [];

    while (str.length) {
      var ret = clause();
      if (!ret) throw new Error('syntax error near `' + str + '`');
      clauses.push(ret);
    }

    return clauses;
  }

  /**
   * where | avg
   */

  function clause() {
    return where()
      || avg()
      || sum()
      || min()
      || max()
      || map();
  }

  /**
   * "MIN" list?
   */

  function min() {
    if (!accept(/^MIN */i)) return;
    var m = list()
    if (!m) return ['min'];
    return ['min', m];
  }

  /**
   * "MAX" list?
   */

  function max() {
    if (!accept(/^MAX */i)) return;
    var m = list()
    if (!m) return ['max'];
    return ['max', m];
  }

  /**
   * "AVG" list?
   */

  function avg() {
    if (!accept(/^AVG */i)) return;
    var m = list()
    if (!m) return ['avg'];
    return ['avg', m];
  }

  /**
   * "SUM" list?
   */

  function sum() {
    if (!accept(/^SUM */i)) return;
    var m = list()
    if (!m) return ['sum'];
    return ['sum', m];
  }

  /**
   * "MAP" list
   */

  function map() {
    if (!accept(/^MAP */i)) return;
    var m = list()
    if (!m) return;
    return ['map', m];
  }

  /**
   * "WHERE" condition
   * "SELECT" condition
   */

  function where() {
    if (!accept(/^(WHERE|SELECT) */i)) return;
    var cond = condition()
    if (!cond) return;
    return ['where', cond];
  }

  /**
   * "*" | expr
   */

  function condition() {
    if (star()) return '*';
    return expr();
  }

  /**
   * "*"
   */

  function star() {
    return accept(/^(\*) */, 'star');
  }

  /**
   * not
   */

  function expr() {
    var ret = not();
    if (!ret) return;
    return ['expr', ret];
  }

  /**
   *   include "!=" include
   * | include "!" include
   * | include
   */

  function not() {
    var left = include();
    if (!left) return;

    if (accept(/^(!=?) */)) {
      var right = include();
      if (!right) return;
      return ['not', left, right];
    }

    return left;
  }

  /**
   *   eq "~=" eq
   * | eq "~" eq
   * | eq
   */

  function include() {
    var left = eq();
    if (!left) return;

    if (accept(/^(~=?) */)) {
      var right = eq();
      if (!right) return;
      return ['include', left, right];
    }

    return left;
  }

  /**
   *   list "==" member
   * | list "=" member
   * | list
   */

  function eq() {
    var left = list();
    if (!left) return;

    if (accept(/^(==?) */)) {
      var right = member();
      if (!right) return;
      return ['eq', left, right];
    }

    return left;
  }

  /**
   * member (',' member)*
   */

  function list() {
    var ret = member();
    if (!ret) return;

    var arr = [ret];
    while (accept(/^, */)) {
      ret = member();
      if (!ret) return;
      arr.push(ret);
    }

    return 1 == arr.length
      ? arr[0]
      : ['list', arr];
  }

  /**
   *   primitive "." member
   * | primitive
   */

  function member() {
    var left = primitive();
    if (!left) return;

    if ('.' == str[0]) {
      str = str.slice(1);
      return ['member', left, member()];
    }

    return left;
  }

  /**
   *   level
   * | id
   * | int
   * | string
   */

  function primitive() {
    return level()
      || id()
      || int()
      || string();
  }

  /**
   *   sstring
   * | dstring
   */

  function string() {
    return sstring()
      || dstring();
  }

  /**
   * single quoted string.
   */

  function sstring() {
    return accept(/^'([^']*)' */, 'string');
  }

  /**
   * double quoted string.
   */

  function dstring() {
    return accept(/^"([^"]*)" */, 'string');
  }

  /**
   * Log level.
   */

  function level() {
    var m = accept(/^(debug|info|warn|error) */);
    if (!m) return;
    return ['eq', ['id', 'level'], ['string', m[1]]];
  }

  /**
   * integer.
   */

  function int() {
    return accept(/^(\d+) */, 'int');
  }

  /**
   * identifier.
   */

  function id() {
    return accept(/^([a-z][\w-]*) */, 'id');
  }
}
