
/**
 * Module dependencies.
 */

var parse = require('./');

function test(query) {
  console.log();
  console.log(query);
  console.log();
  console.log(require('util').inspect(parse(query), false, 15, true));
  console.log();
}

test('*');
test('error WHERE type = "request"');
test('response WHERE status = 200 AVG duration');
test('response where status = 200 map duration');
test('response select status = 200 map duration avg');
test('request WHERE header.user-agent ~ "Mozilla" MAP duration');
test('upload min duration')
test('upload max duration')
test('upload MAP name, file.size, file.type, duration')